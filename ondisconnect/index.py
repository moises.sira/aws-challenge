
import json
import os
import logging

def lambda_handler(event, context):
    logger.debug("ondisconnect: %s" % event)

    return { 'statusCode': 200,
             'body': 'ok' }