
## declare an array variable
declare -a arr=("avion" "auto" "casa" "comprar" "trabajar" "vender" "camion" "barco")

## now loop through the above array
for i in "${arr[@]}"
do
    aws dynamodb put-item \
    --table-name RandomWords \
    --item '{
        "word": {"S": '\""${i}"\"'}
      }' \
    --return-consumed-capacity TOTAL
   echo 
done
