import json
import os
import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

def lambda_handler(event, context):
    logger.debug("onconnect: %s" % event)

    return { 'statusCode': 200,
             'body': 'ok' }