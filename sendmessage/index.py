
import json
import os
import logging
import boto3
import json
import random

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(os.environ['TABLE_NAME'])

def lambda_handler(event, context):
    logger.debug("onconnect: %s" % event)

    connection_id = event.get('requestContext',{}).get('connectionId')
    domain_name = event.get('requestContext',{}).get('domainName')
    stage       = event.get('requestContext',{}).get('stage')

    response = table.scan()
    data = response['Items']
    pos = random.randint(0, len(data) - 1)

    apigw_management = boto3.client('apigatewaymanagementapi',
                                    endpoint_url=F"https://{domain_name}/{stage}")
    apigw_management.post_to_connection(ConnectionId=connection_id,Data=json.dumps(data[pos]))

    return { 'statusCode': 200, 'body': 'ok' }