# challenge websockets-aws-dynamo-lamda-python3.8

This is the code and template for the Websocket-api-RandomWords.  There are three functions contained within the directories and a SAM template that wires them up to a DynamoDB table and a bash script to fill the table to return the api a random word:

# Requeriments

--> Python 3.8

--> bash

--> AWS-CLI (configured with credentials)

--> AWS SAM CLI

# Deploying to your AWS account

You need to have configured the AWS-CLi and and the AWS SAM CLI.

```bash
run the command for deploy:

sam deploy --guided
```

```bash
and this one for populte the table in dynamo

./dynamo_fill.sh

```
## Testing the WebSocket API

To test the WebSocket API, you can use wscat or websocat, an open-source command line tool.

1. On the console, connect to your published API endpoint by executing the following command:
``` bash
$ websocat wss://{YOUR-API-ID}.execute-api.{YOUR-REGION}.amazonaws.com/{STAGE}
```
or

``` bash
$ wscat -c wss://{YOUR-API-ID}.execute-api.{YOUR-REGION}.amazonaws.com/{STAGE}
```
2. To test the sendMessage function, send a JSON message like the following example. The Lambda function sends it back using the callback URL: 

``` bash
$ websocat -c wss://{YOUR-API-ID}.execute-api.{YOUR-REGION}.amazonaws.com/{STAGE}

> {"action":"sendmessage", "data":"word"}
< {"word": "camion"}
```
or: 

``` bash
$ wscat -c wss://{YOUR-API-ID}.execute-api.{YOUR-REGION}.amazonaws.com/{STAGE}
connected (press CTRL+C to quit)
> {"action":"sendmessage", "data":"word"}
< {"word": "camion"}
```